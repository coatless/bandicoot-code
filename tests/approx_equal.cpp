// Copyright 2023 Ryan Curtin (http://www.ratml.org/)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// ------------------------------------------------------------------------

#include <armadillo>
#include <bandicoot>
#include "catch.hpp"

using namespace coot;

TEMPLATE_TEST_CASE("approx_equal_trivial", "[approx_equal]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> A = randi<Mat<eT>>(3, 3, distr_param(1, 10));
  Mat<eT> B = A;

  REQUIRE( approx_equal(A, B, "absdiff", 1e-5)       == true );
  REQUIRE( approx_equal(A, B, "reldiff", 1e-5)       == true );
  REQUIRE( approx_equal(A, B, "absdiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(A, B, "reldiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(A, B, "both",    1e-5, 1e-5) == true );

  REQUIRE( approx_equal(B, A, "absdiff", 1e-5)       == true );
  REQUIRE( approx_equal(B, A, "reldiff", 1e-5)       == true );
  REQUIRE( approx_equal(B, A, "absdiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(B, A, "reldiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(B, A, "both",    1e-5, 1e-5) == true );

  B += 10;

  REQUIRE( approx_equal(A, B, "absdiff", 1e-5)       == false );
  REQUIRE( approx_equal(A, B, "reldiff", 1e-5)       == false );
  REQUIRE( approx_equal(A, B, "absdiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(A, B, "reldiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(A, B, "both",    1e-5, 1e-5) == false );

  REQUIRE( approx_equal(B, A, "absdiff", 1e-5)       == false );
  REQUIRE( approx_equal(B, A, "reldiff", 1e-5)       == false );
  REQUIRE( approx_equal(B, A, "absdiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(B, A, "reldiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(B, A, "both",    1e-5, 1e-5) == false );
  }



TEST_CASE("approx_equal_empty", "[approx_equal]")
  {
  fmat A, B;

  REQUIRE( approx_equal(A, B, "absdiff", 1e-5)       == true );
  REQUIRE( approx_equal(A, B, "reldiff", 1e-5)       == true );
  REQUIRE( approx_equal(A, B, "absdiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(A, B, "reldiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(A, B, "both",    1e-5, 1e-5) == true );
  }



TEST_CASE("approx_equal_invalid_method", "[approx_equal]")
  {
  fmat A(10, 10, fill::randu);
  fmat B(10, 10, fill::randu);

  bool result;
  REQUIRE_THROWS( result = approx_equal(A, B, "what", 1e-5) );
  }



TEST_CASE("approx_equal_different_size_1", "[approx_equal]")
  {
  fmat A(5, 3, fill::randu);
  fmat B = A.submat(0, 0, 3, 2);

  REQUIRE( approx_equal(A, B, "absdiff", 1e-5)       == false );
  REQUIRE( approx_equal(A, B, "reldiff", 1e-5)       == false );
  REQUIRE( approx_equal(A, B, "absdiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(A, B, "absdiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(A, B, "both",    1e-5, 1e-5) == false );
  }



TEST_CASE("approx_equal_different_size_2", "[approx_equal]")
  {
  fmat A(5, 3, fill::randu);
  fmat B;

  REQUIRE( approx_equal(A, B, "absdiff", 1e-5)       == false );
  REQUIRE( approx_equal(A, B, "reldiff", 1e-5)       == false );
  REQUIRE( approx_equal(A, B, "absdiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(A, B, "absdiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(A, B, "both",    1e-5, 1e-5) == false );

  REQUIRE( approx_equal(B, A, "absdiff", 1e-5)       == false );
  REQUIRE( approx_equal(B, A, "reldiff", 1e-5)       == false );
  REQUIRE( approx_equal(B, A, "absdiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(B, A, "absdiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(B, A, "both",    1e-5, 1e-5) == false );
  }



TEMPLATE_TEST_CASE("approx_equal_rand", "[approx_equal]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  for (size_t trial = 0; trial < 10; ++trial)
    {
    Mat<eT> A = randi<Mat<eT>>(1000, 200, distr_param(1, 100));
    Mat<eT> B = randi<Mat<eT>>(1000, 200, distr_param(1, 100));

    arma::Mat<eT> A_cpu(A);
    arma::Mat<eT> B_cpu(B);

    REQUIRE( approx_equal(A, B, "absdiff", 1e-5)       == arma::approx_equal(A_cpu, B_cpu, "absdiff", 1e-5      ) );
    REQUIRE( approx_equal(A, B, "reldiff", 1e-5)       == arma::approx_equal(A_cpu, B_cpu, "reldiff", 1e-5      ) );
    REQUIRE( approx_equal(A, B, "absdiff", 1e-5, 1e-5) == arma::approx_equal(A_cpu, B_cpu, "absdiff", 1e-5, 1e-5) );
    REQUIRE( approx_equal(A, B, "absdiff", 1e-5, 1e-5) == arma::approx_equal(A_cpu, B_cpu, "reldiff", 1e-5, 1e-5) );
    REQUIRE( approx_equal(A, B, "both",    1e-5, 1e-5) == arma::approx_equal(A_cpu, B_cpu, "both",    1e-5, 1e-5) );

    REQUIRE( approx_equal(B, A, "absdiff", 1e-5)       == arma::approx_equal(B_cpu, A_cpu, "absdiff", 1e-5      ) );
    REQUIRE( approx_equal(B, A, "reldiff", 1e-5)       == arma::approx_equal(B_cpu, A_cpu, "reldiff", 1e-5      ) );
    REQUIRE( approx_equal(B, A, "absdiff", 1e-5, 1e-5) == arma::approx_equal(B_cpu, A_cpu, "absdiff", 1e-5, 1e-5) );
    REQUIRE( approx_equal(B, A, "absdiff", 1e-5, 1e-5) == arma::approx_equal(B_cpu, A_cpu, "reldiff", 1e-5, 1e-5) );
    REQUIRE( approx_equal(B, A, "both",    1e-5, 1e-5) == arma::approx_equal(B_cpu, A_cpu, "both",    1e-5, 1e-5) );
    }
  }



TEMPLATE_TEST_CASE("approx_equal_add_random_noise", "[approx_equal]", float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> A(250, 750, fill::randu);
  Mat<eT> B = A;

  REQUIRE( approx_equal(A, B, "absdiff", 1e-5)       == true );
  REQUIRE( approx_equal(A, B, "reldiff", 1e-5)       == true );
  REQUIRE( approx_equal(A, B, "absdiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(A, B, "reldiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(A, B, "both",    1e-5, 1e-5) == true );

  REQUIRE( approx_equal(B, A, "absdiff", 1e-5)       == true );
  REQUIRE( approx_equal(B, A, "reldiff", 1e-5)       == true );
  REQUIRE( approx_equal(B, A, "absdiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(B, A, "reldiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(B, A, "both",    1e-5, 1e-5) == true );

  A += 1;
  B += 1;
  B += 1e-7; // also add a little bit extra so they are not exactly the same

  REQUIRE( approx_equal(A, B, "absdiff", 1e-3)       == true );
  REQUIRE( approx_equal(A, B, "reldiff", 1e-3)       == true );
  REQUIRE( approx_equal(A, B, "absdiff", 1e-3, 1e-3) == true );
  REQUIRE( approx_equal(A, B, "reldiff", 1e-3, 1e-3) == true );
  REQUIRE( approx_equal(A, B, "both",    1e-3, 1e-3) == true );

  REQUIRE( approx_equal(B, A, "absdiff", 1e-3)       == true );
  REQUIRE( approx_equal(B, A, "reldiff", 1e-3)       == true );
  REQUIRE( approx_equal(B, A, "absdiff", 1e-3, 1e-3) == true );
  REQUIRE( approx_equal(B, A, "reldiff", 1e-3, 1e-3) == true );
  REQUIRE( approx_equal(B, A, "both",    1e-3, 1e-3) == true );
  }



TEMPLATE_TEST_CASE("approx_equal_abstol_checks", "[approx_equal]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> A(1, 1);
  Mat<eT> B(1, 1);

  A(0, 0) = (eT) 1;
  B(0, 0) = (eT) 3;

  REQUIRE( approx_equal(A, B, "absdiff", 3)       == true );
  REQUIRE( approx_equal(A, B, "absdiff", 3, 1e-5) == true );
  REQUIRE( approx_equal(B, A, "absdiff", 3)       == true );
  REQUIRE( approx_equal(B, A, "absdiff", 3, 1e-5) == true );

  REQUIRE( approx_equal(A, B, "absdiff", 1)       == false );
  REQUIRE( approx_equal(A, B, "absdiff", 1, 1e-5) == false );
  REQUIRE( approx_equal(B, A, "absdiff", 1)       == false );
  REQUIRE( approx_equal(B, A, "absdiff", 1, 1e-5) == false );
  }



TEMPLATE_TEST_CASE("approx_equal_abstol_checks_2", "[approx_equal]", float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> A(1, 1);
  Mat<eT> B(1, 1);

  A(0, 0) = (eT) 0.0;
  B(0, 0) = (eT) 0.1;

  REQUIRE( approx_equal(A, B, "absdiff", 0.101)       == true );
  REQUIRE( approx_equal(A, B, "absdiff", 0.101, 1e-5) == true );
  REQUIRE( approx_equal(B, A, "absdiff", 0.101)       == true );
  REQUIRE( approx_equal(B, A, "absdiff", 0.101, 1e-5) == true );

  REQUIRE( approx_equal(A, B, "absdiff", 0.099)       == false );
  REQUIRE( approx_equal(A, B, "absdiff", 0.099, 1e-5) == false );
  REQUIRE( approx_equal(B, A, "absdiff", 0.099)       == false );
  REQUIRE( approx_equal(B, A, "absdiff", 0.099, 1e-5) == false );
  }



TEMPLATE_TEST_CASE("approx_equal_reltol_checks", "[approx_equal]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> A(1, 1);
  Mat<eT> B(1, 1);

  A(0, 0) = (eT) 1;
  B(0, 0) = (eT) 3;

  REQUIRE( approx_equal(A, B, "reldiff", 1)       == true );
  REQUIRE( approx_equal(A, B, "reldiff", 1e-5, 1) == true );
  REQUIRE( approx_equal(B, A, "reldiff", 1)       == true );
  REQUIRE( approx_equal(B, A, "reldiff", 1e-5, 1) == true );
  }



TEMPLATE_TEST_CASE("approx_equal_reltol_checks_2", "[approx_equal]", float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> A(1, 1);
  Mat<eT> B(1, 1);

  A(0, 0) = (eT) 10.1;
  B(0, 0) = (eT) 10.2;

  REQUIRE( approx_equal(A, B, "reldiff", 0.01)         == true );
  REQUIRE( approx_equal(A, B, "reldiff", 0.01)         == true );
  REQUIRE( approx_equal(B, A, "reldiff", 0.0001, 0.01) == true );
  REQUIRE( approx_equal(B, A, "reldiff", 0.0001, 0.01) == true );

  REQUIRE( approx_equal(A, B, "reldiff", 0.0097)      == false );
  REQUIRE( approx_equal(A, B, "reldiff", 0.0097)      == false );
  REQUIRE( approx_equal(B, A, "reldiff", 0.1, 0.0097) == false );
  REQUIRE( approx_equal(B, A, "reldiff", 0.1, 0.0097) == false );
  }



TEMPLATE_TEST_CASE("approx_equal_both_diff_tols", "[approx_equal]", float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> A(1, 1);
  Mat<eT> B(1, 1);

  A(0, 0) = (eT) 10.1;
  B(0, 0) = (eT) 10.2;

  // Use the smallest possible passing tolerances.
  REQUIRE( approx_equal(A, B, "both", 0.101, 0.01) == true );
  REQUIRE( approx_equal(B, A, "both", 0.101, 0.01) == true );

  // Break the absolute tolerance.
  REQUIRE( approx_equal(A, B, "both", 0.099, 0.01) == false );
  REQUIRE( approx_equal(B, A, "both", 0.099, 0.01) == false );

  // Break the relative tolerance.
  REQUIRE( approx_equal(A, B, "both", 0.101, 0.0097) == false );
  REQUIRE( approx_equal(B, A, "both", 0.101, 0.0097) == false );

  // Break both tolerances.
  REQUIRE( approx_equal(A, B, "both", 0.099, 0.0097) == false );
  REQUIRE( approx_equal(B, A, "both", 0.099, 0.0097) == false );
  }



TEMPLATE_TEST_CASE("approx_equal_subviews", "[approx_equal]", u32, s32, u64, s64, float, double)
  {

  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> A = randi<Mat<eT>>(100, 250, distr_param(1, 10));
  Mat<eT> C = randi<Mat<eT>>(100, 250, distr_param(1, 10));
  Mat<eT> D = C;
  D.submat(3, 3, 5, 5) = A.submat(3, 3, 5, 5);
  D.submat(7, 7, 9, 9) = A.submat(3, 3, 5, 5);

  REQUIRE( approx_equal(A.submat(3, 3, 5, 5), D.submat(4, 4, 6, 6), "absdiff", 1e-5)       == false );
  REQUIRE( approx_equal(A.submat(3, 3, 5, 5), D.submat(4, 4, 6, 6), "reldiff", 1e-5)       == false );
  REQUIRE( approx_equal(A.submat(3, 3, 5, 5), D.submat(4, 4, 6, 6), "absdiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(A.submat(3, 3, 5, 5), D.submat(4, 4, 6, 6), "reldiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(A.submat(3, 3, 5, 5), D.submat(4, 4, 6, 6), "both",    1e-5, 1e-5) == false );

  REQUIRE( approx_equal(D.submat(4, 4, 6, 6), A.submat(3, 3, 5, 5), "absdiff", 1e-5)       == false );
  REQUIRE( approx_equal(D.submat(4, 4, 6, 6), A.submat(3, 3, 5, 5), "reldiff", 1e-5)       == false );
  REQUIRE( approx_equal(D.submat(4, 4, 6, 6), A.submat(3, 3, 5, 5), "absdiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(D.submat(4, 4, 6, 6), A.submat(3, 3, 5, 5), "reldiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(D.submat(4, 4, 6, 6), A.submat(3, 3, 5, 5), "both",    1e-5, 1e-5) == false );

  REQUIRE( approx_equal(A.submat(3, 3, 5, 5), D.submat(3, 3, 5, 5), "absdiff", 1e-5)       == true );
  REQUIRE( approx_equal(A.submat(3, 3, 5, 5), D.submat(3, 3, 5, 5), "reldiff", 1e-5)       == true );
  REQUIRE( approx_equal(A.submat(3, 3, 5, 5), D.submat(3, 3, 5, 5), "absdiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(A.submat(3, 3, 5, 5), D.submat(3, 3, 5, 5), "reldiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(A.submat(3, 3, 5, 5), D.submat(3, 3, 5, 5), "both",    1e-5, 1e-5) == true );

  REQUIRE( approx_equal(D.submat(3, 3, 5, 5), A.submat(3, 3, 5, 5), "absdiff", 1e-5)       == true );
  REQUIRE( approx_equal(D.submat(3, 3, 5, 5), A.submat(3, 3, 5, 5), "reldiff", 1e-5)       == true );
  REQUIRE( approx_equal(D.submat(3, 3, 5, 5), A.submat(3, 3, 5, 5), "absdiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(D.submat(3, 3, 5, 5), A.submat(3, 3, 5, 5), "reldiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(D.submat(3, 3, 5, 5), A.submat(3, 3, 5, 5), "both",    1e-5, 1e-5) == true );

  REQUIRE( approx_equal(A.submat(3, 3, 5, 5), D.submat(7, 7, 9, 9), "absdiff", 1e-5)       == true );
  REQUIRE( approx_equal(A.submat(3, 3, 5, 5), D.submat(7, 7, 9, 9), "reldiff", 1e-5)       == true );
  REQUIRE( approx_equal(A.submat(3, 3, 5, 5), D.submat(7, 7, 9, 9), "absdiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(A.submat(3, 3, 5, 5), D.submat(7, 7, 9, 9), "reldiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(A.submat(3, 3, 5, 5), D.submat(7, 7, 9, 9), "both",    1e-5, 1e-5) == true );

  REQUIRE( approx_equal(D.submat(7, 7, 9, 9), A.submat(3, 3, 5, 5), "absdiff", 1e-5)       == true );
  REQUIRE( approx_equal(D.submat(7, 7, 9, 9), A.submat(3, 3, 5, 5), "reldiff", 1e-5)       == true );
  REQUIRE( approx_equal(D.submat(7, 7, 9, 9), A.submat(3, 3, 5, 5), "absdiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(D.submat(7, 7, 9, 9), A.submat(3, 3, 5, 5), "reldiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(D.submat(7, 7, 9, 9), A.submat(3, 3, 5, 5), "both",    1e-5, 1e-5) == true );
  }



TEMPLATE_TEST_CASE("approx_equal_vector_subviews", "[approx_equal]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Col<eT> A = randi<Col<eT>>(10000, distr_param(1, 10));
  Col<eT> B = A;
  Col<eT> C = randi<Col<eT>>(10000, distr_param(1, 10));
  Col<eT> D = C;
  D.subvec(0, 5000) = A.subvec(0, 5000);

  REQUIRE( approx_equal(A, B, "absdiff", 1e-5)       == true );
  REQUIRE( approx_equal(A, B, "reldiff", 1e-5)       == true );
  REQUIRE( approx_equal(A, B, "absdiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(A, B, "reldiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(A, B, "both",    1e-5, 1e-5) == true );

  REQUIRE( approx_equal(B, A, "absdiff", 1e-5)       == true );
  REQUIRE( approx_equal(B, A, "reldiff", 1e-5)       == true );
  REQUIRE( approx_equal(B, A, "absdiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(B, A, "reldiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(B, A, "both",    1e-5, 1e-5) == true );

  REQUIRE( approx_equal(A, C, "absdiff", 1e-5)       == false );
  REQUIRE( approx_equal(A, C, "reldiff", 1e-5)       == false );
  REQUIRE( approx_equal(A, C, "absdiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(A, C, "reldiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(A, C, "both",    1e-5, 1e-5) == false );

  REQUIRE( approx_equal(C, A, "absdiff", 1e-5)       == false );
  REQUIRE( approx_equal(C, A, "reldiff", 1e-5)       == false );
  REQUIRE( approx_equal(C, A, "absdiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(C, A, "reldiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(C, A, "both",    1e-5, 1e-5) == false );

  REQUIRE( approx_equal(A, D, "absdiff", 1e-5)       == false );
  REQUIRE( approx_equal(A, D, "reldiff", 1e-5)       == false );
  REQUIRE( approx_equal(A, D, "absdiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(A, D, "reldiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(A, D, "both",    1e-5, 1e-5) == false );

  REQUIRE( approx_equal(D, A, "absdiff", 1e-5)       == false );
  REQUIRE( approx_equal(D, A, "reldiff", 1e-5)       == false );
  REQUIRE( approx_equal(D, A, "absdiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(D, A, "reldiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(D, A, "both",    1e-5, 1e-5) == false );

  REQUIRE( approx_equal(A.subvec(100, 250), B.subvec(100, 250), "absdiff", 1e-5)       == true );
  REQUIRE( approx_equal(A.subvec(100, 250), B.subvec(100, 250), "reldiff", 1e-5)       == true );
  REQUIRE( approx_equal(A.subvec(100, 250), B.subvec(100, 250), "absdiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(A.subvec(100, 250), B.subvec(100, 250), "reldiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(A.subvec(100, 250), B.subvec(100, 250), "absdiff", 1e-5)       == true );

  REQUIRE( approx_equal(B.subvec(100, 250), A.subvec(100, 250), "reldiff", 1e-5)       == true );
  REQUIRE( approx_equal(B.subvec(100, 250), A.subvec(100, 250), "absdiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(B.subvec(100, 250), A.subvec(100, 250), "reldiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(B.subvec(100, 250), A.subvec(100, 250), "both",    1e-5, 1e-5) == true );
  REQUIRE( approx_equal(B.subvec(100, 250), A.subvec(100, 250), "both",    1e-5, 1e-5) == true );

  REQUIRE( approx_equal(A.subvec(100, 250), C.subvec(100, 250), "absdiff", 1e-5)       == false );
  REQUIRE( approx_equal(A.subvec(100, 250), C.subvec(100, 250), "reldiff", 1e-5)       == false );
  REQUIRE( approx_equal(A.subvec(100, 250), C.subvec(100, 250), "absdiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(A.subvec(100, 250), C.subvec(100, 250), "reldiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(A.subvec(100, 250), C.subvec(100, 250), "absdiff", 1e-5)       == false );

  REQUIRE( approx_equal(C.subvec(100, 250), A.subvec(100, 250), "reldiff", 1e-5)       == false );
  REQUIRE( approx_equal(C.subvec(100, 250), A.subvec(100, 250), "absdiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(C.subvec(100, 250), A.subvec(100, 250), "reldiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(C.subvec(100, 250), A.subvec(100, 250), "both",    1e-5, 1e-5) == false );
  REQUIRE( approx_equal(C.subvec(100, 250), A.subvec(100, 250), "both",    1e-5, 1e-5) == false );

  REQUIRE( approx_equal(A.subvec(100, 250), D.subvec(100, 250), "absdiff", 1e-5)       == true );
  REQUIRE( approx_equal(A.subvec(100, 250), D.subvec(100, 250), "reldiff", 1e-5)       == true );
  REQUIRE( approx_equal(A.subvec(100, 250), D.subvec(100, 250), "absdiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(A.subvec(100, 250), D.subvec(100, 250), "reldiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(A.subvec(100, 250), D.subvec(100, 250), "absdiff", 1e-5)       == true );

  REQUIRE( approx_equal(D.subvec(100, 250), A.subvec(100, 250), "reldiff", 1e-5)       == true );
  REQUIRE( approx_equal(D.subvec(100, 250), A.subvec(100, 250), "absdiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(D.subvec(100, 250), A.subvec(100, 250), "reldiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(D.subvec(100, 250), A.subvec(100, 250), "both",    1e-5, 1e-5) == true );
  REQUIRE( approx_equal(D.subvec(100, 250), A.subvec(100, 250), "both",    1e-5, 1e-5) == true );

  REQUIRE( approx_equal(D.subvec(100, 5010), A.subvec(100, 5010), "reldiff", 1e-5)       == false );
  REQUIRE( approx_equal(D.subvec(100, 5010), A.subvec(100, 5010), "absdiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(D.subvec(100, 5010), A.subvec(100, 5010), "reldiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(D.subvec(100, 5010), A.subvec(100, 5010), "both",    1e-5, 1e-5) == false );
  REQUIRE( approx_equal(D.subvec(100, 5010), A.subvec(100, 5010), "both",    1e-5, 1e-5) == false );

  REQUIRE( approx_equal(A.subvec(100, 5010), D.subvec(100, 5010), "absdiff", 1e-5)       == false );
  REQUIRE( approx_equal(A.subvec(100, 5010), D.subvec(100, 5010), "reldiff", 1e-5)       == false );
  REQUIRE( approx_equal(A.subvec(100, 5010), D.subvec(100, 5010), "absdiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(A.subvec(100, 5010), D.subvec(100, 5010), "reldiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(A.subvec(100, 5010), D.subvec(100, 5010), "absdiff", 1e-5)       == false );
  }



TEMPLATE_TEST_CASE("approx_equal_row_subviews", "[approx_equal]", u32, s32, u64, s64, float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Row<eT> A = randi<Row<eT>>(10000, distr_param(1, 10));
  Row<eT> B = A;
  Row<eT> C = randi<Row<eT>>(10000, distr_param(1, 10));
  Row<eT> D = C;
  D.subvec(0, 5000) = A.subvec(0, 5000);

  REQUIRE( approx_equal(A, B, "absdiff", 1e-5)       == true );
  REQUIRE( approx_equal(A, B, "reldiff", 1e-5)       == true );
  REQUIRE( approx_equal(A, B, "absdiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(A, B, "reldiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(A, B, "both",    1e-5, 1e-5) == true );

  REQUIRE( approx_equal(B, A, "absdiff", 1e-5)       == true );
  REQUIRE( approx_equal(B, A, "reldiff", 1e-5)       == true );
  REQUIRE( approx_equal(B, A, "absdiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(B, A, "reldiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(B, A, "both",    1e-5, 1e-5) == true );

  REQUIRE( approx_equal(A, C, "absdiff", 1e-5)       == false );
  REQUIRE( approx_equal(A, C, "reldiff", 1e-5)       == false );
  REQUIRE( approx_equal(A, C, "absdiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(A, C, "reldiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(A, C, "both",    1e-5, 1e-5) == false );

  REQUIRE( approx_equal(C, A, "absdiff", 1e-5)       == false );
  REQUIRE( approx_equal(C, A, "reldiff", 1e-5)       == false );
  REQUIRE( approx_equal(C, A, "absdiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(C, A, "reldiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(C, A, "both",    1e-5, 1e-5) == false );

  REQUIRE( approx_equal(A, D, "absdiff", 1e-5)       == false );
  REQUIRE( approx_equal(A, D, "reldiff", 1e-5)       == false );
  REQUIRE( approx_equal(A, D, "absdiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(A, D, "reldiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(A, D, "both",    1e-5, 1e-5) == false );

  REQUIRE( approx_equal(D, A, "absdiff", 1e-5)       == false );
  REQUIRE( approx_equal(D, A, "reldiff", 1e-5)       == false );
  REQUIRE( approx_equal(D, A, "absdiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(D, A, "reldiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(D, A, "both",    1e-5, 1e-5) == false );

  REQUIRE( approx_equal(A.subvec(100, 250), B.subvec(100, 250), "absdiff", 1e-5)       == true );
  REQUIRE( approx_equal(A.subvec(100, 250), B.subvec(100, 250), "reldiff", 1e-5)       == true );
  REQUIRE( approx_equal(A.subvec(100, 250), B.subvec(100, 250), "absdiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(A.subvec(100, 250), B.subvec(100, 250), "reldiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(A.subvec(100, 250), B.subvec(100, 250), "absdiff", 1e-5)       == true );

  REQUIRE( approx_equal(B.subvec(100, 250), A.subvec(100, 250), "reldiff", 1e-5)       == true );
  REQUIRE( approx_equal(B.subvec(100, 250), A.subvec(100, 250), "absdiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(B.subvec(100, 250), A.subvec(100, 250), "reldiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(B.subvec(100, 250), A.subvec(100, 250), "both",    1e-5, 1e-5) == true );
  REQUIRE( approx_equal(B.subvec(100, 250), A.subvec(100, 250), "both",    1e-5, 1e-5) == true );

  REQUIRE( approx_equal(A.subvec(100, 250), C.subvec(100, 250), "absdiff", 1e-5)       == false );
  REQUIRE( approx_equal(A.subvec(100, 250), C.subvec(100, 250), "reldiff", 1e-5)       == false );
  REQUIRE( approx_equal(A.subvec(100, 250), C.subvec(100, 250), "absdiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(A.subvec(100, 250), C.subvec(100, 250), "reldiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(A.subvec(100, 250), C.subvec(100, 250), "absdiff", 1e-5)       == false );

  REQUIRE( approx_equal(C.subvec(100, 250), A.subvec(100, 250), "reldiff", 1e-5)       == false );
  REQUIRE( approx_equal(C.subvec(100, 250), A.subvec(100, 250), "absdiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(C.subvec(100, 250), A.subvec(100, 250), "reldiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(C.subvec(100, 250), A.subvec(100, 250), "both",    1e-5, 1e-5) == false );
  REQUIRE( approx_equal(C.subvec(100, 250), A.subvec(100, 250), "both",    1e-5, 1e-5) == false );

  REQUIRE( approx_equal(A.subvec(100, 250), D.subvec(100, 250), "absdiff", 1e-5)       == true );
  REQUIRE( approx_equal(A.subvec(100, 250), D.subvec(100, 250), "reldiff", 1e-5)       == true );
  REQUIRE( approx_equal(A.subvec(100, 250), D.subvec(100, 250), "absdiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(A.subvec(100, 250), D.subvec(100, 250), "reldiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(A.subvec(100, 250), D.subvec(100, 250), "absdiff", 1e-5)       == true );

  REQUIRE( approx_equal(D.subvec(100, 250), A.subvec(100, 250), "reldiff", 1e-5)       == true );
  REQUIRE( approx_equal(D.subvec(100, 250), A.subvec(100, 250), "absdiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(D.subvec(100, 250), A.subvec(100, 250), "reldiff", 1e-5, 1e-5) == true );
  REQUIRE( approx_equal(D.subvec(100, 250), A.subvec(100, 250), "both",    1e-5, 1e-5) == true );
  REQUIRE( approx_equal(D.subvec(100, 250), A.subvec(100, 250), "both",    1e-5, 1e-5) == true );

  REQUIRE( approx_equal(D.subvec(100, 5010), A.subvec(100, 5010), "reldiff", 1e-5)       == false );
  REQUIRE( approx_equal(D.subvec(100, 5010), A.subvec(100, 5010), "absdiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(D.subvec(100, 5010), A.subvec(100, 5010), "reldiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(D.subvec(100, 5010), A.subvec(100, 5010), "both",    1e-5, 1e-5) == false );
  REQUIRE( approx_equal(D.subvec(100, 5010), A.subvec(100, 5010), "both",    1e-5, 1e-5) == false );

  REQUIRE( approx_equal(A.subvec(100, 5010), D.subvec(100, 5010), "absdiff", 1e-5)       == false );
  REQUIRE( approx_equal(A.subvec(100, 5010), D.subvec(100, 5010), "reldiff", 1e-5)       == false );
  REQUIRE( approx_equal(A.subvec(100, 5010), D.subvec(100, 5010), "absdiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(A.subvec(100, 5010), D.subvec(100, 5010), "reldiff", 1e-5, 1e-5) == false );
  REQUIRE( approx_equal(A.subvec(100, 5010), D.subvec(100, 5010), "absdiff", 1e-5)       == false );
  }
