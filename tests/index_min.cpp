// Copyright 2019 Ryan Curtin (http://www.ratml.org/)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// ------------------------------------------------------------------------

#include <armadillo>
#include <bandicoot>
#include "catch.hpp"

using namespace coot;

TEMPLATE_TEST_CASE("index_min_small", "[index_min]", float, double, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Col<eT> x(16);
  for (uword i = 0; i < 16; ++i)
    x[i] = i + 1;

  uword min_index = index_min(x);

  REQUIRE( min_index == uword(0) );
  }



TEMPLATE_TEST_CASE("index_min_1", "[index_min]", float, double, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Col<eT> x(6400);
  for (uword i = 0; i < 6400; ++i)
    x[i] = (6400 - i);

  uword min_index = index_min(x);

  REQUIRE( min_index == uword(6399) );
  }



TEMPLATE_TEST_CASE("index_min_strange_size", "[index_min]", float, double, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Col<eT> x(608);

  for(uword i = 0; i < 608; ++i)
    x[i] = i + 1;
  x[277] = eT(0);

  uword min_index = index_min(x);

  REQUIRE( min_index == uword(277) );
  }



TEMPLATE_TEST_CASE("index_min_large", "[index_min]", float, double, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  arma::Col<eT> cpu_x = arma::conv_to<arma::Col<eT>>::from(arma::randu<arma::Col<double>>(100000) * 10.0);
  cpu_x.randu();
  Col<eT> x(cpu_x);

  uword cpu_min_index = index_min(cpu_x);
  uword min_index = index_min(x);

  REQUIRE(min_index == cpu_min_index);
  }



TEMPLATE_TEST_CASE("index_min_2", "[index_min]", float, double)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Col<eT> x(50);
  x.randu();
  x += eT(1);

  uword min_index = index_min(x);

  REQUIRE( min_index >= uword(0) );
  REQUIRE( min_index < uword(50) );
  }



TEMPLATE_TEST_CASE("index_min_vec_expr", "[index_min]", float, double, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x(50, 60, fill::randu);

  const uword min_index = index_min(3 + 2 * vectorise(x));

  Col<eT> z = 3 + 2 * vectorise(x);
  const uword min_index_ref = index_min(z);

  REQUIRE( min_index == min_index_ref );
  }



TEMPLATE_TEST_CASE("index_min_colwise_1", "[index_min]", float, double, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x(10, 10);
  for (uword c = 0; c < 10; ++c)
    {
    for (uword r = 0; r < 10; ++r)
      {
      if (r == c)
        {
        x(r, c) = 0;
        }
      else
        {
        x(r, c) = c + 10;
        }
      }
    }

  Mat<uword> s = index_min(x, 0);

  REQUIRE( s.n_rows == 1  );
  REQUIRE( s.n_cols == 10 );
  for (uword c = 0; c < 10; ++c)
    {
    REQUIRE( uword(s[c]) == c );
    }
  }



TEMPLATE_TEST_CASE("index_min_colwise_2", "[index_min]", float, double, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x(10, 10);
  for (uword c = 0; c < 10; ++c)
    {
    for (uword r = 0; r < 10; ++r)
      {
      if (r == c)
        {
        x(r, c) = 0;
        }
      else
        {
        x(r, c) = r + 100;
        }
      }
    }

  Mat<uword> s = index_min(x, 0);

  REQUIRE( s.n_rows == 1  );
  REQUIRE( s.n_cols == 10 );
  for (uword c = 0; c < 10; ++c)
    {
    REQUIRE( uword(s[c]) == c );
    }
  }



TEMPLATE_TEST_CASE("index_min_rowwise_1", "[index_min]", float, double, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x(10, 10);
  for (uword c = 0; c < 10; ++c)
    {
    for (uword r = 0; r < 10; ++r)
      {
      if (r == c)
        {
        x(r, c) = 0;
        }
      else
        {
        x(r, c) = c + 10;
        }
      }
    }

  Mat<uword> s = index_min(x, 1);

  REQUIRE( s.n_rows == 10 );
  REQUIRE( s.n_cols == 1  );
  for (uword r = 0; r < 10; ++r)
    {
    REQUIRE( uword(s[r]) == r );
    }
  }



TEMPLATE_TEST_CASE("index_min_rowwise_2", "[index_min]", float, double, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x(10, 10);
  for (uword c = 0; c < 10; ++c)
    {
    for (uword r = 0; r < 10; ++r)
      {
      if (r == c)
        {
        x(r, c) = 0;
        }
      else
        {
        x(r, c) = r + 100;
        }
      }
    }

  Mat<uword> s = index_min(x, 1);

  REQUIRE( s.n_rows == 10 );
  REQUIRE( s.n_cols == 1  );
  for (uword r = 0; r < 10; ++r)
    {
    REQUIRE( uword(s[r]) == r );
    }
  }



TEMPLATE_TEST_CASE("subview_index_min_colwise_1", "[index_min]", float, double, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x(10, 10);
  for (uword c = 0; c < 10; ++c)
    {
    for (uword r = 0; r < 10; ++r)
      {
      if (r == c)
        {
        x(r, c) = 0;
        }
      else
        {
        x(r, c) = c + 100;
        }
      }
    }

  Mat<uword> s = index_min(x.submat(1, 1, 8, 8), 0);

  REQUIRE( s.n_rows == 1 );
  REQUIRE( s.n_cols == 8 );
  for (uword c = 0; c < 8; ++c)
    {
    REQUIRE( uword(s[c]) == c );
    }
  }



TEMPLATE_TEST_CASE("subview_index_min_colwise_2", "[index_min]", float, double, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x(10, 10);
  for (uword c = 0; c < 10; ++c)
    {
    for (uword r = 0; r < 10; ++r)
      {
      x(r, c) = r;
      }
    }

  Mat<uword> s = index_min(x.submat(1, 1, 8, 8), 0);

  REQUIRE( s.n_rows == 1 );
  REQUIRE( s.n_cols == 8 );
  for (uword c = 0; c < 8; ++c)
    {
    REQUIRE( uword(s[c]) == 0 );
    }
  }



TEMPLATE_TEST_CASE("subview_index_min_colwise_full", "[index_min]", float, double, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x(10, 10);
  for (uword c = 0; c < 10; ++c)
    {
    for (uword r = 0; r < 10; ++r)
      {
      if (r == c)
        {
        x(r, c) = 0;
        }
      else
        {
        x(r, c) = c + 100;
        }
      }
    }

  Mat<uword> s = index_min(x.submat(0, 0, 9, 9), 0);

  REQUIRE( s.n_rows == 1  );
  REQUIRE( s.n_cols == 10 );
  for (uword c = 0; c < 10; ++c)
    {
    REQUIRE( uword(s[c]) == c );
    }
  }



TEMPLATE_TEST_CASE("subview_index_min_rowwise_1", "[index_min]", float, double, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x(10, 10);
  for (uword c = 0; c < 10; ++c)
    {
    for (uword r = 0; r < 10; ++r)
      {
      x(r, c) = c;
      }
    }

  Mat<uword> s = index_min(x.submat(1, 1, 8, 8), 1);

  REQUIRE( s.n_rows == 8 );
  REQUIRE( s.n_cols == 1 );
  for (uword r = 0; r < 8; ++r)
    {
    REQUIRE( uword(s[r]) == 0 );
    }
  }



TEMPLATE_TEST_CASE("subview_index_min_rowwise_2", "[index_min]", float, double, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x(10, 10);
  for (uword c = 0; c < 10; ++c)
    {
    for (uword r = 0; r < 10; ++r)
      {
      if (r == c)
        {
        x(r, c) = 0;
        }
      else
        {
        x(r, c) = 100 + r;
        }
      }
    }

  Mat<uword> s = index_min(x.submat(1, 1, 8, 8), 1);

  REQUIRE( s.n_rows == 8 );
  REQUIRE( s.n_cols == 1 );
  for (uword r = 0; r < 8; ++r)
    {
    REQUIRE( uword(s[r]) == r );
    }
  }



TEMPLATE_TEST_CASE("subview_index_min_rowwise_full", "[index_min]", float, double, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x(10, 10);
  for (uword c = 0; c < 10; ++c)
    {
    for (uword r = 0; r < 10; ++r)
      {
      if (r == c)
        {
        x(r, c) = 0;
        }
      else
        {
        x(r, c) = r + 100;
        }
      }
    }

  Mat<uword> s = index_min(x.submat(0, 0, 9, 9), 1);

  REQUIRE( s.n_rows == 10 );
  REQUIRE( s.n_cols == 1  );
  for (uword r = 0; r < 10; ++r)
    {
    REQUIRE( uword(s[r]) == r );
    }
  }



TEST_CASE("alias_index_min_colwise", "[index_min]")
  {
  Mat<uword> x = randi<Mat<uword>>(100, 100, distr_param(0, 100000));
  Mat<uword> y(x);

  Mat<uword> r1 = index_min(y, 0);
  x = index_min(x, 0);

  REQUIRE( x.n_rows == r1.n_rows );
  REQUIRE( x.n_cols == r1.n_cols );

  for (uword c = 0; c < r1.n_cols; ++c)
    {
    for (uword r = 0; r < r1.n_rows; ++r)
      {
      REQUIRE( uword(x(r, c)) == uword(r1(r, c)) );
      }
    }
  }



TEST_CASE("alias_index_min_rowwise", "[index_min]")
  {
  Mat<uword> x = randi<Mat<uword>>(100, 100, distr_param(0, 100000));
  Mat<uword> y(x);

  Mat<uword> r1 = index_min(y, 1);
  x = index_min(x, 1);

  REQUIRE( x.n_rows == r1.n_rows );
  REQUIRE( x.n_cols == r1.n_cols );

  for (uword c = 0; c < r1.n_cols; ++c)
    {
    for (uword r = 0; r < r1.n_rows; ++r)
      {
      REQUIRE( uword(x(r, c)) == uword(r1(r, c)) );
      }
    }
  }



TEMPLATE_TEST_CASE("member_index_min", "[index_min]", float, double, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x(100, 100, fill::randu);

  uword index_min1 = index_min(vectorise(x));
  uword index_min2 = x.index_min();

  REQUIRE( index_min1 == index_min2 );

  Col<eT> y(100, fill::randu);

  index_min1 = index_min(y);
  index_min2 = y.index_min();

  REQUIRE( index_min1 == index_min2 );

  Row<eT> z(100, fill::randu);

  index_min1 = index_min(z);
  index_min2 = z.index_min();

  REQUIRE( index_min1 == index_min2 );
  }



TEMPLATE_TEST_CASE("member_min_with_index", "[index_min]", float, double, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x(60, 100, fill::randu);

  uword index1;
  uword index2;
  uword row_index;
  uword col_index;

  index1 = index_min(vectorise(x));
  const eT min_val1 = x[index1];

  const eT min_val2 = x.min(index2);

  const eT min_val3 = x.min(row_index, col_index);
  const uword index3 = row_index + col_index * x.n_rows;

  REQUIRE( min_val1 == min_val2 );
  REQUIRE( min_val1 == min_val3 );
  REQUIRE( index1 == index2 );
  REQUIRE( index1 == index3 );
  }



TEMPLATE_TEST_CASE("expr_member_index_min", "[index_min]", float, double, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x(100, 50, fill::randu);
  Mat<eT> y(50, 100, fill::randu);

  Mat<eT> z = (3.0 + (x % y.t()));

  uword index_min1 = (3.0 + (x % y.t())).index_min();
  uword index_min2 = z.index_min();

  REQUIRE( index_min1 == index_min2 );
  }



TEMPLATE_TEST_CASE("expr_member_min_with_index", "[index_min]", float, double, u32, s32, u64, s64)
  {
  typedef TestType eT;

  if (!coot_rt_t::is_supported_type<eT>())
    {
    return;
    }

  Mat<eT> x(100, 50, fill::randu);
  Mat<eT> y(50, 100, fill::randu);

  Mat<eT> z = (3.0 + (x % y.t()));

  uword index1;
  uword index2;
  uword row_index;
  uword col_index;

  index1 = index_min(vectorise(z));
  const eT min_val1 = z[index1];

  const eT min_val2 = (3.0 + (x % y.t())).min(index2);

  const eT min_val3 = (3.0 + (x % y.t())).min(row_index, col_index);
  const uword index3 = row_index + col_index * x.n_rows;

  REQUIRE( min_val1 == min_val2 );
  REQUIRE( min_val1 == min_val3 );
  REQUIRE( index1 == index2 );
  REQUIRE( index1 == index3 );
  }
